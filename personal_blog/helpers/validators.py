from django.forms import ValidationError


def at_least_3(string):
    if len(string) < 3:
        raise ValidationError('The value is too short!!!')
