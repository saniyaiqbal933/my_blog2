from django.urls import path
from ..views.post_views import (
    PostDetailView,
    PostCreateView,
    PostListView,
    PostUpdateView,
    PostDeleteView
    )

from ..views.comment_views import CommentCreateView, CommentUpdateView, CommentDeleteView

app_name = 'posts'

urlpatterns = [
    path('<int:pk>/detail/', PostDetailView.as_view(), name='detail'),
    path('create/', PostCreateView.as_view(), name='create'),
    path('list/', PostListView.as_view(), name='list'),
    path('<int:pk>/update/', PostUpdateView.as_view(), name='update'),
    path('<int:pk>/delete/', PostDeleteView.as_view(), name='delete'),

    path('<int:post_pk>/detail/comment/create/', CommentCreateView.as_view(), name='comment_create'),
    path('<int:post_pk>/detail/comment/update/', CommentUpdateView.as_view(), name='comment_update'),
    path('<int:post_pk>/detail/comment/delete/', CommentDeleteView.as_view(), name='comment_delete'),
]
