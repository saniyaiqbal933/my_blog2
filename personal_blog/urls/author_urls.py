from django.urls import path
from ..views.author_views import (
    AuthorListView,
    AuthorCreateView,
    AuthorUpdateView,
    AuthorDeleteView
)

urlpatterns = [
    path('create/', AuthorCreateView.as_view(), name='author_create'),
    path('list/', AuthorListView.as_view(), name='author_list'),
    path('<int:pk>/update/', AuthorUpdateView.as_view(), name='author_update'),
    path('<int:pk>/delete/', AuthorDeleteView.as_view(), name='author_delete'),
]
