from django import forms
from django.forms import ValidationError
from .models import Comment, Author, Post


# class CommentForm(forms.Form):
#     text = forms.CharField(
#         max_length=1000, 
#         required=True, 
#         label="Comment",
#         widget=widgets.Textarea(attrs={
#             "class": "form-control"
#         })
#         )
#     author = forms.CharField(
#         max_length=50,
#         required=False,
#         label="Author",
#         widget=widgets.TextInput(attrs={
#             'class': 'form-control', 
#             'placeholder': 'Author name'
#         }),
#         initial="Anonymous"
#     )


class PostForm(forms.ModelForm):
    # title = forms.CharField(
    #     max_length=200, 
    #     required=True, 
    #     label="Title",
    #     widget=widgets.TextInput(attrs={
    #         'class': 'form-control', 
    #         'placeholder': 'Title'}),
    #     validators=[at_least_3]
    #     )
    # author = forms.ModelChoiceField(
    #     required=True,
    #     label="Author",
    #     widget=widgets.Select(attrs={
    #         'class': 'form-select', 
    #         'placeholder': 'Author'}),
    #     queryset=Author.objects.all()
    #     )
    # body = forms.CharField(
    #     max_length=3000, 
    #     required=False, 
    #     label="Body",
    #     widget=widgets.Textarea(attrs={'class': 'form-control', 'rows': 4}),
    #     validators=(at_least_3,)
    #     )
    # tags = forms.ModelMultipleChoiceField(
    #     required=False,
    #     label="Tags",
    #     queryset=Tag.objects.all(),
    #     widget=widgets.SelectMultiple(attrs={'class': 'form-select'})
    # )

    class Meta:
        model = Post
        exclude = ['author']
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Title'}),
            'body': forms.Textarea(attrs={'class': 'form-control', 'rows': 4}),
            'author': forms.Select(attrs={
                'class': 'form-select',
                'placeholder': 'Author'}),
            'tags': forms.SelectMultiple(attrs={'class': 'form-select'})
        }

    def clean(self):
        cleaned_data = super().clean()

        if cleaned_data.get('body') == cleaned_data.get('title'):
            raise ValidationError("Post title and post body should not be similar!")
        return cleaned_data

    def clean_title(self):
        title = self.cleaned_data.get('title')
        if len(title) < 5:
            raise ValidationError('Title is too short!')
        return title


class AuthorForm(forms.ModelForm):
    class Meta:
        model = Author
        fields = ['name']
        widgets = {'name': forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Author name'
        })}


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['text']
        widgets = {
            'text': forms.Textarea(attrs={
                'class': 'form-control'}),
            'author': forms.TextInput(attrs={
                'class': 'form-control'})
        }


class SearchForm(forms.Form):
    search = forms.CharField(max_length=100, required=False, label='Search')
