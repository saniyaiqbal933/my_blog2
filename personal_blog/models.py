from django.db import models
from .helpers.validators import at_least_3
from django.contrib.auth import get_user_model


class Author(models.Model):
    name = models.CharField(max_length=200, null=False, blank=False, verbose_name='Author name')

    def __str__(self) -> str:
        return self.name


class Post(models.Model):
    title = models.CharField(max_length=200, null=False, blank=False, verbose_name='Title')
    body = models.TextField(max_length=3000, null=True, blank=True, verbose_name='Text', validators=(at_least_3,))
    author = models.ForeignKey(
        get_user_model(), 
        on_delete=models.SET_DEFAULT,
        default=1, 
        null=False, 
        related_name='posts',
        verbose_name='Author'
        )
    tags = models.ManyToManyField(
        'personal_blog.Tag',
        related_name='posts',
        blank=True
    )
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Creation date and time')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Update date and time')

    def __str__(self):
        return f"#{self.pk} - {self.title}"
    
    class Meta:
        permissions = [
            ('can_read_post', 'Can read post')
        ]


class Comment(models.Model):
    text = models.TextField(max_length=1000, null=False, blank=False, verbose_name='Comment')
    author = models.ForeignKey(
        get_user_model(), 
        null=True, 
        blank=True, 
        on_delete=models.SET_DEFAULT,
        default=1, 
        related_name='comments',
        verbose_name='Author'
        )
    post = models.ForeignKey('personal_blog.Post', on_delete=models.CASCADE, related_name='comments',
                             verbose_name='Post')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Creation date and time')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Update date and time')

    def __str__(self) -> str:
        return self.text[:20]


class Tag(models.Model):
    name = models.CharField(max_length=30, verbose_name='Tag')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Creation date and time')

    def __str__(self) -> str:
        return self.name
