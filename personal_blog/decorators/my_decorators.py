from time import time, sleep

def time_decorator(fn):
    def wrapper_function(*args):
        start = time()
        fn(*args)
        stop = time()
        execution_time = stop - start
        print('execution_time:' , execution_time, '\n')
    return wrapper_function

@time_decorator
def func_add(a, b):
    sleep(0.5)
    return a + b