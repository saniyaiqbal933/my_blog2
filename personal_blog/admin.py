from django.contrib import admin
from .models import Post, Tag


class PostAdmin(admin.ModelAdmin):
    list_display = ['title', 'author', 'created_at']
    list_filter = ['author']
    search_fields = ['title', 'body']
    fields = ['title', 'body', 'author', 'created_at', 'updated_at']
    readonly_fields = ['created_at', 'updated_at']

class TagAdmin(admin.ModelAdmin):
    fields = ['name']

admin.site.register(Post, PostAdmin)
admin.site.register(Tag, TagAdmin)
