from ..models import Author
from ..forms import AuthorForm
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy


# def author_create_view(request):
#     if request.method == 'GET':
#         form = AuthorForm()
#         return render(request, 'authors/create.html', context={'form': form})
#     elif request.method == 'POST':
#         form = AuthorForm(request.POST)
#         if form.is_valid():
#             author = Author.objects.create(
#                 name=request.POST.get('name')
#             )
#             return redirect('author_list')
#         else:
#             return render(request, 'authors/create.html', context={'form': form})
    
# def author_list_view(request):
#     return render(request, 'authors/list.html', context={'authors': Author.objects.all()})


# class AuthorView(View):
#     def dispatch(self, request, *args, **kwargs):
#         if request.method == 'GET':
#             return self.get(request, *args, **kwargs)
#         if request.method == 'POST':
#             return self.post(request, *args, **kwargs)
    
#     def get(self, request, *args, **kwargs):
#         form = AuthorForm()
#         return render(request, 'authors/create.html', context={'form': form})

#     def post(self, request, *args, **kwargs):
#         form = AuthorForm(request.POST)
#         if form.is_valid():
#             author = Author.objects.create(
#                 name=request.POST.get('name')
#             )
#             return redirect('author_list')
#         else:
#             return render(request, 'authors/create.html', context={'form': form})


class AuthorListView(ListView):
    template_name = 'authors/list.html'
    model = Author
    context_object_name = 'authors'
    ordering = ['name']
    paginate_by = 10
    paginate_orphans = 1

    def get_queryset(self):
        queryset = super().get_queryset()
        sort_by = self.request.GET.get('sort_by')
        if sort_by:
            queryset = queryset.order_by(sort_by)
        return queryset


class AuthorCreateView(CreateView):
    template_name = 'authors/create.html'
    model = Author
    form_class = AuthorForm
    success_url = reverse_lazy('author_list')


class AuthorDeleteView(DeleteView):
    model = Author
    success_url = reverse_lazy('author_list')

    def get(self, request, *args, **kwargs):
        return self.delete(request, *args, **kwargs)
    
    
class AuthorUpdateView(UpdateView):
    model = Author
    template_name = 'authors/update.html'
    form_class = AuthorForm
    context_object_name = 'author'
    success_url = reverse_lazy('author_list')
