from django.shortcuts import redirect
from django.urls import reverse, reverse_lazy
from ..models import Post
from ..forms import PostForm, CommentForm, SearchForm
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.utils.http import urlencode
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.core.exceptions import PermissionDenied


# def index_view(request):
#     return render(request, 'index.html')


# def post_list_view(request):
#     posts = Post.objects.all()
#     return render(request, 'posts/list.html', context={
#         'posts': posts
#     })


# def post_detail_view(request, *args, **kwargs):
#     post = get_object_or_404(Post, pk=kwargs.get('pk'))
#     return render(request, 'posts/detail.html', 
#                   context={'post': post})


# def post_create_view(request):
#     if request.method == 'GET':
#         form = PostForm()
#         return render(request, 'posts/create.html', context={
#             'form': form,
#             'authors': Author.objects.all()
#             })
#     elif request.method == 'POST':
#         form = PostForm(request.POST)
#         if form.is_valid():
#             author = Author.objects.get(pk=request.POST.get('author'))
#             tags = form.cleaned_data.pop('tags')
#             new_post = Post.objects.create(
#                 title=form.cleaned_data['title'],
#                 body=form.cleaned_data['body'],
#                 author=author
#             )
#             new_post.tags.set(tags)
#             return redirect('post_list')
#         else:
#             return render(request, 'posts/create.html', context={
#                 'form': form
#             })


# def post_update_view(request, *args, **kwargs):
#     post = get_object_or_404(Post, pk=kwargs.get('pk'))

#     if request.method == 'GET':
#         form = PostForm(initial={
#             'title': post.title,
#             'body': post.body,
#             'author': post.author,
#             'tags': post.tags.all()
#         })
#         return render(request, 'posts/update.html', context={'form': form, 'post': post})
#     elif request.method == 'POST':
#         form = PostForm(data=request.POST, initial={'tags': post.tags.all()})
#         if form.is_valid():
#             tags = form.cleaned_data.pop('tags')
#             post.title = form.cleaned_data.get('title')
#             post.body = form.cleaned_data.get('body')
#             post.author = form.cleaned_data.get('author')
#             post.save()
#             post.tags.set(tags)
#             return redirect('post_detail', pk=post.pk)
#         else:
#             return render(request, 'posts/update.html', context={'form': form, 'post': post})


# class PostDetailView(View):
#     def get(self, request, *args, **kwargs):
#         form = CommentForm()
#         post = get_object_or_404(Post, pk=kwargs.get('pk'))
#         return render(request, 'posts/detail.html', 
#                   context={'post': post, 'form': form})


# class PostCreateView(CustomFormView):
#     template_name = 'posts/create.html'
#     form_class = PostForm

#     def form_valid(self, form):
#         # data = {}
#         # tags = form.cleaned_data.pop('tags')

#         # for key, value in form.cleaned_data.items():
#         #     if value is not None:
#         #         data[key] = value
#         # self.post = Post.objects.create(**data)
#         # self.post.tags.set(tags)
#         self.post = form.save()
#         return super().form_valid(form)
    
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['authors'] = Author.objects.all()
#         return context
    
#     def get_redirect_url(self):
#         return reverse('post_list')

# def post_delete_view(request, *args, **kwargs):
#     post = get_object_or_404(Post, pk=kwargs.get('pk'))
#     if request.method == 'GET':
#         return render(request, 'posts/delete.html', context={'post': post})
#     elif request.method == 'POST':
#         post.delete()
#         return redirect('home_page')

# class PostUpdateView(FormView):
#     template_name = 'posts/update.html'
#     form_class = PostForm

#     def dispatch(self, request: HttpRequest, *args, **kwargs):
#         self.my_post = self.get_object()
#         return super().dispatch(request, *args, **kwargs)
    
#     def get_object(self):
#         pk = self.kwargs.get('pk')
#         return get_object_or_404(Post, pk=pk)
    
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['post'] = self.my_post
#         return context
    
#     # def get_initial(self) -> dict[str, Any]:
#     #     initial = {}

#     #     for key in ['title', 'body', 'author', 'tags']:
#     #         initial[key] = getattr(self.post, key)
#     #     initial['tags'] = self.post.tags.all()
#     #     return initial

#     def get_form_kwargs(self):
#         kwargs = super().get_form_kwargs()
#         kwargs['instance'] = self.my_post
#         return kwargs
    
#     def form_valid(self, form):
#         # tags = form.cleaned_data.pop('tags')
#         # for key, value in form.cleaned_data.items():
#         #     if value:
#         #         setattr(self.post, key, value)
#         # self.post.save()
#         # self.post.tags.set(tags)
#         self.my_post = form.save()
#         return super().form_valid(form)
    
#     def get_success_url(self) -> str:
#         return reverse('post_detail', kwargs={'pk': self.my_post.pk})


class PostListView(ListView):
    template_name = 'posts/list.html'
    model = Post
    context_object_name = 'posts'
    form = SearchForm
    paginate_by = 5
    paginate_orphans = 1
    search_value = None    # Добавил данное свойство, чтобы IDE не ругался на self.search_value в методе get()
    
    def get(self, request, *args, **kwargs):
        self.form = self.get_search_form()
        self.search_value = self.get_search_value()
        return super().get(request, *args, **kwargs)
    
    def get_search_form(self):
        return self.form(self.request.GET)
    
    def get_search_value(self):
        if self.form.is_valid():
            return self.form.cleaned_data.get('search')
        return None
    
    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['form'] = self.form
        if self.search_value:
            context['query'] = urlencode({'search': self.search_value})
        return context
    
    def get_queryset(self):
        queryset = super().get_queryset()
        if self.search_value:
            query = Q(title__icontains=self.search_value) | Q(author__name__icontains=self.search_value)
            queryset = queryset.filter(query)
        return queryset


class PostDetailView(DetailView):
    template_name = 'posts/detail.html'
    model = Post

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        comment_form = CommentForm()
        my_post = self.object
        comments = my_post.comments.order_by('-created_at')
        context['comments'] = comments
        context['comment_form'] = comment_form
        return context
        

class PostCreateView(LoginRequiredMixin, CreateView):
    template_name = 'posts/create.html'
    model = Post
    form_class = PostForm

    # def dispatch(self, request, *args, **kwargs):
    #     if not request.user.is_authenticated:
    #         return redirect('login')
    #     return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)
    
    def get_success_url(self):
        return reverse('posts:detail', kwargs={'pk': self.object.pk})
    

class PostUpdateView(PermissionRequiredMixin, UpdateView):
    model = Post
    template_name = 'posts/update.html'
    form_class = PostForm
    context_object_name = 'post'
    permission_required = ['personal_blog.change_post', 'personal_blog.can_read_post']

    # def dispatch(self, request, *args, **kwargs):
    #     user = request.user
    #     if not user.is_authenticated:
    #         return redirect('home_page')
    #     if not user.has_perm('personal_blog.change_post'):
    #         raise PermissionDenied
    #     return super().dispatch(request, *args, **kwargs)


    def get_success_url(self):
        return reverse('posts:detail', kwargs={'pk': self.object.pk})


class PostDeleteView(DeleteView):
    template_name = 'posts/delete.html'
    model = Post
    context_object_name = 'post'
    success_url = reverse_lazy('posts:list')
