from django.shortcuts import get_object_or_404, redirect
from ..forms import CommentForm
from ..models import Comment, Post
from django.views.generic import CreateView, UpdateView, DeleteView
from django.urls import reverse


# class CommentView(View):
#     def post(self, request, *args, **kwargs):
#         form = CommentForm(request.POST)
#         post_pk = kwargs.get('post_pk')
#         if form.is_valid():
#             post = get_object_or_404(Post, pk=post_pk)
#             post.comments.create(
#                 text=request.POST.get("text"),
#                 author=request.POST.get("author")
#                 )
#             return redirect('post_detail', post_pk)

# class CommentDeleteView(CustomDeleteView):
#     model = Comment
#     confirm_deletion = False
#     key_kwarg = 'post_pk'

#     def get_redirect_url(self):
#         return reverse('post_detail', kwargs={'pk': self.object.post.pk})


class CommentCreateView(CreateView):
    model = Comment
    form_class = CommentForm
    template_name = 'comments/create.html'

    def form_valid(self, form):
        my_post = get_object_or_404(Post, pk=self.kwargs.get('post_pk'))
        form.instance.post = my_post
        form.instance.author = self.request.user
        return super().form_valid(form)
    
    def get_success_url(self) -> str:
        return reverse('posts:detail', kwargs={'pk': self.object.post.pk})
    

class CommentUpdateView(UpdateView):
    model = Comment
    template_name = 'comments/update.html'
    context_object_name = 'comment'
    form_class = CommentForm
    pk_url_kwarg = 'post_pk'

    def get_success_url(self):
        return reverse('posts:detail', kwargs={'pk': self.object.post.pk})
    

class CommentDeleteView(DeleteView):
    model = Comment
    pk_url_kwarg = 'post_pk'

    def get(self, request, *args, **kwargs):
        return self.delete(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('posts:detail', kwargs={'pk': self.object.post.pk})
    