from django.views.generic import TemplateView


# class IndexView(View):
#     def get(self, request, *args, **kwargs):
#         return render(request, 'index.html')


class IndexView(TemplateView):
    template_name = 'index.html'
